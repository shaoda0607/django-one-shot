from django.shortcuts import (
    get_list_or_404,
    render,
    get_object_or_404,
    redirect,
)
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {"todo_list": todo_list}
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            saved_form = form.save()
            return redirect("todo_list_detail", saved_form.id)

    form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/todo_list_create.html", context)


def todo_list_edit(request, id):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            editing_list = get_object_or_404(TodoList, id=id)
            for key, value in request.POST.items():
                setattr(editing_list, key, value)
            editing_list.save()
            return redirect("todo_list_detail", id)

    editing_list = get_object_or_404(TodoList, id=id)
    form = TodoListForm(instance=editing_list)
    context = {"form": form}
    return render(request, "todos/todo_list_edit.html", context)

def todo_list_delete(request, id):
    if request.method == "POST":
        deleting_list = get_object_or_404(TodoList, id=id)
        deleting_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_list_delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            saved_form = form.save()
            return redirect("todo_list_detail", saved_form.list.id)

    form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/todo_item_create.html", context)

def todo_item_update(request, id):
    editing_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=editing_item)
        if form.is_valid():
            saved_form = form.save()
            return redirect("todo_list_detail", saved_form.list.id)

    form = TodoItemForm(instance=editing_item)
    context = {
        "form": form
    }
    return render(request, "todos/todo_item_edit.html", context)
