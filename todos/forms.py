from django.forms import ModelForm, DateInput, DateTimeField
from todos.models import TodoItem, TodoList
from django.contrib.admin import widgets


class CalendarInput(DateInput):
    input_type = "datetime-local"


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
        widgets = {"due_date": CalendarInput(format="%Y-%m-%dT%H:%M")}
